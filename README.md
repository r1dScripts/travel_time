travel_time is a small program in python 3 which aims to provide the duration of travel between two points.  

It was written to gives the information about the traffic between work and home.

```
 ./travel_time.py -h  
usage: travel_time.py [-h] [-d ] [-j ]

Send travel time between Work and Home to a XMPP jid

optional arguments:
  -h, --help            show this help message and exit
  -d [], --direction []
                        Direction: work to home or home to work 
                        h2w : home to work
                        w2h : work to home
  -j [], --jid []       jid of the contact (the recipient of the message)
```

Dependencies:  

  * googlemaps : google maps python api
  * sleekxmpp  : a library used to send the message with the help of the XMPP protocol.  

First, you need to create a simple config file containing the following informations:

```
jid of the server
password
recipient
```
where

  * jid of the server is an account created for this usage.
  * password is the the associated password of the account
  * recipient is your jid


For now, the file need to be named ```send_xmpprc``` and it need to be placed in the same folder than the script.   

Secondly, you need to create the file credentials.py with your information. Use credentials.example.py as an example.

The script sends a summary of the travel time, lengths (if you need to take a detour) with and without traffic.

![gajim capture](http://pix.toile-libre.org/upload/original/1477472800.png)