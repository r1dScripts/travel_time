#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# """
# @author: Arnaud Joset
#
# This file is part of TravelTime.
#
# TravelTime is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TravelTime is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with TravelTime.  If not, see <http://www.gnu.org/licenses/>.
#

api_key = 'KEY'
work_location = "42 Work Street Bigtown 4242 United States"
home_location = "42 Home Street Cosytown 4242 United States"
# The summary string if the best roads are taken (no traffic)
summary_key = "Big Autoroute name for example"
# The shortest distance (km)
d = 42
# the time threshold above which you get a warning (minutes)
t = 42
