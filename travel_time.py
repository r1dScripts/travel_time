#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# """
# @author: Arnaud Joset
#
# This file is part of TravelTime.
#
# TravelTime is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TravelTime is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with TravelTime.  If not, see <http://www.gnu.org/licenses/>.
#

import logging
import googlemaps
import xmpp
import argparse
import time
from datetime import datetime
import credentials
import sys
import os

# work DIR is the directory where the script is
os.chdir(sys.path[0])

logging.basicConfig(filename='infotraffic.log', level=logging.INFO)


# gmaps = googlemaps.Client(key='Add Your Key here')
# start_location = ""
# stop_location = ""

def get_start_stop(direction):
    start = None
    stop = None
    if direction == "w2h":
        direction = "work to home"
        start = credentials.work_location
        stop = credentials.home_location
    if direction == "h2w":
        direction = "home to work"
        start = credentials.home_location
        stop = credentials.work_location
    return start, stop, direction


def get_traject_duration(direction):
    summary_msg = ''
    gmaps = googlemaps.Client(key=credentials.api_key)
    start, stop, direction = get_start_stop(direction)
#    geocode_start = gmaps.geocode(start)
#    geocode_stop = gmaps.geocode(stop)

    # Request directions via public transit
    now = datetime.now()
    route = gmaps.directions(start,
                             stop,
                             traffic_model='best_guess',
                             departure_time=now)

    route_res = route[0]
    summary = route_res['summary']
    direction_time = route_res['legs'][0]['duration_in_traffic']
    distance = route_res['legs'][0]['distance']

    summary_msg += '\n' + direction
    summary_msg += '\n' + str(summary) + ' ' + "Distance: {} km \n".format((round(distance['value']/1000,2)))
    if credentials.summary_key not in summary:
        summary_msg += "Eviter " + credentials.summary_key + '\n'

    if distance['value'] > credentials.d * 1000:
        summary_msg += "Chemin non habituel" + '\n'
    else:
        summary_msg += "Chemin habituel" + '\n'

    if direction_time['value'] >= credentials.t * 60:
        summary_msg += "Attention bouchons" + '\n'
    else:
        summary_msg += "Pas de soucis sur la route" + '\n'
    traffic_time = route_res['legs'][0]['duration_in_traffic']['value']
    normal_time =  route_res['legs'][0]['duration']['value']
    summary_msg += "temps traffic: {} min".format(round(traffic_time/60.0,2))  + '\n'
    summary_msg += "temps normal: {} min".format(round(normal_time/60.0,2)) + '\n'

    return summary_msg


def read_parameters(file):
    with open(file, "r") as f:
        parameters = []
        for line in f:
            line = line.rstrip('\r\n')
            parameters.append(line)
        jid = parameters[0]
        password = parameters[1]
        to = parameters[2]
        return jid, password, to

def send_infotraffic(direction, jid_arg=None):
    file = '/home/arnaud/.config/xmpp/send_xmpprc_memo'
    file = 'send_xmpprc'
    jid, password, to = read_parameters(file)
    jid += '/infotraffic'

    if jid_arg is not None:
        jid = jid_arg
    # print(jid,password, to)
    if jid is None or password is None or to is None:
        print("Error in config file")
        logging.error("Error in config file")
        return 1
    summary_msg = get_traject_duration(direction)
    xmpp_instance = xmpp.SendMsgBot(jid, password, to, summary_msg)
    xmpp_instance.register_plugin('xep_0030')  # Service Discovery
    xmpp_instance.register_plugin('xep_0199')  # XMPP Ping
    time.sleep(3)
    xmpp_instance.connect()
    time.sleep(5)
    if xmpp_instance.connect():
        # block=True will block the current thread. By default, block=False
        xmpp_instance.process()
    else:
        logging.error("Unable to connect.")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Send travel time between Work and Home to a XMPP jid')
    parser.add_argument('-d', '--direction', dest='direction', metavar='', nargs='?', action='store', default="h2w",
                        help='Direction: work to home or home to work' + '\n' +\
                             "h2w : home to work " + '\n' +\
                             "w2h : work to home")

    parser.add_argument('-j', '--jid', dest='jid', metavar='', nargs='?', action='store',
                        help='jid of the contact (the recipient of the message)')
    args = parser.parse_args()
    print(args)
    direction = args.direction
    jid = args.jid
    send_infotraffic(direction, jid)
